var toggleButton = document.querySelector('.toggle-button')
var mobileNav = document.querySelector('.mobile-nav')
var backButton = document.querySelector('.back-button')

toggleButton.addEventListener('click', function(){
    mobileNav.setAttribute('style' , 'display : block');
})

backButton.addEventListener('click', function(){
    mobileNav.setAttribute('style' , 'display : none');
})